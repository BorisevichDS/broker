﻿using Consumer.Interfaces;
using Consumer.Settings;
using Core.Brokers.RabbitMQ;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Threading;

namespace Consumer
{
    /// <summary>
    /// Потребитель сообщений.
    /// </summary>
    internal class Consumer : IConsumer
    {
        private readonly IReceiver receiver;
        private readonly int timeoutToReceive;

        public Consumer()
        {
            var configuration = new ConfigurationBuilder()
                 .SetBasePath(Directory.GetCurrentDirectory())
                 .AddJsonFile("AppSettings.json")
                 .Build();

            var brokerSettings = configuration.GetSection("Broker").Get<BrokerSettings>();
            var queueSettings = configuration.GetSection("Broker").GetSection("Queue").Get<QueueSettings>();
            this.timeoutToReceive = configuration.GetValue<int>("TimeoutToReceive");

            var client = new RabbitMQClient(brokerSettings.Host, brokerSettings.VirtualHost, brokerSettings.Login, brokerSettings.Password);
            RabbitMQQueueConfigurator.Configure(client, queueSettings.Name, queueSettings.Durable);
            this.receiver = new RabbitMQReceiver(client, queueSettings.Name);
        }

        /// <summary>
        /// Запускает получение сообщений.
        /// </summary>
        public void Run()
        {
            while (true)
            {
                this.receiver.Receive();
                Thread.Sleep(timeoutToReceive);
            }
        }
    }
}
