﻿namespace Consumer.Interfaces
{
    /// <summary>
    /// Потребитель сообщений.
    /// </summary>
    public interface IConsumer
    {
        /// <summary>
        /// Запускает получение сообщений.
        /// </summary>
        void Run();
    }
}
