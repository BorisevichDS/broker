﻿namespace Consumer.Interfaces
{
    /// <summary>
    /// Получатель.
    /// </summary>
    public interface IReceiver
    {
        /// <summary>
        /// Получить сообщение.
        /// </summary>
        void Receive();
    }
}
