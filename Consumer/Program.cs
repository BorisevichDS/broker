﻿using Consumer.Interfaces;
using System;

namespace Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IConsumer consumer = new Consumer();
                consumer.Run();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }
    }
}
