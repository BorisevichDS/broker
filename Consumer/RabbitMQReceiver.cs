﻿using Consumer.Interfaces;
using Core.Brokers.RabbitMQ;
using Core.Entities;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Text.Json;

namespace Consumer
{
    /// <summary>
    /// Получатель сообщений из брокера сообщений RabbitMQ.
    /// </summary>
    internal class RabbitMQReceiver : IReceiver
    {
        private readonly RabbitMQClient client;
        private readonly string queueName;

        public RabbitMQReceiver(RabbitMQClient client, string queueName)
        {
            this.client = client;
            this.queueName = queueName;
        }

        public void Receive()
        {
            Console.WriteLine($"{DateTime.Now}: Waiting messages...");
            var consumer = new EventingBasicConsumer(this.client.Channel);
            consumer.Received += (ch, ea) =>
            {
                string message = Encoding.UTF8.GetString(ea.Body.ToArray());
                User user = JsonSerializer.Deserialize<User>(message);

                if (user.Email != null)
                {
                    Console.WriteLine($"{DateTime.Now}: received user '{user}'");
                    this.client.Channel.BasicAck(ea.DeliveryTag, false);
                }
            };
            
            this.client.Channel.BasicConsume(this.queueName, false, consumer);
        }
    }
}
