﻿namespace Consumer.Settings
{
    /// <summary>
    /// Настройки для подключения к брокеру сообщений.
    /// </summary>
    internal class BrokerSettings
    {
        /// <summary>
        /// Получает или задает URL адрес, где расположен брокер.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Получает или задает виртуальный хост.
        /// </summary>
        public string VirtualHost { get; set; }

        /// <summary>
        /// Получает или задает логин.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Получает или задает пароль.
        /// </summary>
        public string Password { get; set; }
    }
}
