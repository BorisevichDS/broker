﻿namespace Consumer.Settings
{
    /// <summary>
    /// Настройки очереди.
    /// </summary>
    internal class QueueSettings
    {
        /// <summary>
        /// Получает или задает название очереди.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получает или задает признак сохранения данных в очереди.
        /// </summary>
        public bool Durable { get; set; }
    }
}
