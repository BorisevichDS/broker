﻿using RabbitMQ.Client;
using System;

namespace Core.Brokers.RabbitMQ
{
    public class RabbitMQClient : IDisposable
    {
        private readonly IConnection connection;
        private readonly IModel channel;

        /// <summary>
        /// Канал для доступа к RabbitMQ.
        /// </summary>
        public IModel Channel => channel;

        public RabbitMQClient(string hostName, string virtualHost, string userName, string password)
        {
            var factory = new ConnectionFactory()
            {
                HostName = hostName,
                VirtualHost = virtualHost,
                UserName = userName,
                Password = password
            };
            this.connection = factory.CreateConnection();
            this.channel = connection.CreateModel();
        }

        public void Dispose()
        {
            this.channel.Close();
            this.connection.Close();
            this.Channel.Dispose();
            this.connection.Dispose();
        }

    }
}
