﻿using RabbitMQ.Client;

namespace Core.Brokers.RabbitMQ
{
    /// <summary>
    /// Конфигуратор exchange для RabbitMQ.
    /// </summary>
    public static class RabbitMQExchangeConfigurator
    {
        /// <summary>
        /// Настроить exchange.
        /// </summary>
        /// <param name="client">Клиент для обращения к RabbitMQ.</param>
        /// <param name="name">Имя exchange.</param>
        /// <param name="type">Тип exchange.</param>
        public static void Configure(RabbitMQClient client, string name, string type)
        {
            client.Channel.ExchangeDeclare(exchange: name, type: type);
        }
    }
}
