﻿using RabbitMQ.Client;

namespace Core.Brokers.RabbitMQ
{
    /// <summary>
    /// Связывает exchange и queue.
    /// </summary>
    public static class RabbitMQExchangeQueueBinder
    {
        /// <summary>
        /// Настроить связь между exchange и queue.
        /// </summary>
        /// <param name="client">Клиент.</param>
        /// <param name="exchangeName">Название exchange.</param>
        /// <param name="queueName">Название очереди.</param>
        /// <param name="routingKey">Ключ маршрутизации.</param>
        public static void Configure(RabbitMQClient client, string exchangeName, string queueName, string routingKey)
        {
            client.Channel.QueueBind(queue: queueName, exchange: exchangeName, routingKey: routingKey);
        }
    }
}
