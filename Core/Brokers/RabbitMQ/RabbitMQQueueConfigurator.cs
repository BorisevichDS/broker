﻿namespace Core.Brokers.RabbitMQ
{
    /// <summary>
    /// Конфигуратор очереди для RabbitMQ.
    /// </summary>
    public static class RabbitMQQueueConfigurator
    {
        /// <summary>
        /// Настроить очередь.
        /// </summary>
        /// <param name="client">Клиент.</param>
        /// <param name="queueName">Название очереди.</param>
        /// <param name="durable">Признак сохранения данных.</param>
        public static void Configure(RabbitMQClient client, string queueName, bool durable)
        {
            client.Channel.QueueDeclare(queue: queueName,
                            durable: durable,
                            exclusive: false,
                            autoDelete: false,
                            arguments: null);
        }
    }
}
