﻿namespace Core.Entities
{
    /// <summary>
    /// Пользователь.
    /// </summary>
    public class User
    {
        public User()
        {

        }

        public User(string name, int age, string email)
        {
            this.Name = name;
            this.Age = age;
            this.Email = email;
        }


        /// <summary>
        /// Имя.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Возраст.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Адрес электронной почты.
        /// </summary>
        public string Email { get; set; }

        public override string ToString()
        {
            return $"Name {Name} age {Age} e-mail {Email}";
        }
    }
}
