﻿using Bogus;
using Bogus.Extensions;
using Core.Entities;
using Data.Interfaces;
using System.Collections.Generic;

namespace Data.Generators
{
    /// <summary>
    /// Генератор случайных пользователей.
    /// </summary>
    public class FakeUserGenerator : IDataGenerator<User>
    {
        private readonly Faker<User> faker;

        public FakeUserGenerator()
        {
            faker = new Faker<User>()
                            .CustomInstantiator(u => new User())
                            .RuleFor(p => p.Name, (u, p) => u.Person.FirstName)
                            .RuleFor(p => p.Age, (u, p) => u.Random.Int(0, 100))
                            .RuleFor(p => p.Email, (u, p) => u.Person.Email.OrNull(u, 0.30f));
        }


        /// <summary>
        /// Сгенерировать пользователя.
        /// </summary>
        /// <returns>Пользователь.</returns>
        public User Generate()
        {
            return faker.Generate();
        }

        /// <summary>
        /// Сгенерировать данные пользователей.
        /// </summary>
        /// <param name="count">Количество элементов для генерации.</param>
        /// <returns>Список пользователей.</returns>
        public List<User> Generate(int count)
        {
            return faker.Generate(count);
        }
    }
}
