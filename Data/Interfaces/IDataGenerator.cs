﻿using System.Collections.Generic;

namespace Data.Interfaces
{
    public interface IDataGenerator<T>
    {
        T Generate();

        List<T> Generate(int count);
    }
}
