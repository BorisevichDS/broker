﻿namespace Producer.Interfaces
{
    /// <summary>
    /// Генератор сообщений. 
    /// </summary>
    public interface IProducer
    {
        /// <summary>
        /// Запускает отправку сообщений.
        /// </summary>
        void Run();
    }
}
