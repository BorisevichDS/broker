﻿namespace Producer.Interfaces
{
    /// <summary>
    /// Отправитель.
    /// </summary>
    public interface ISender
    {
        /// <summary>
        /// Отправить сообщение.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        void Send(string message);
    }
}
