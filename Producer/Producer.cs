﻿using Core.Brokers.RabbitMQ;
using Core.Entities;
using Data.Generators;
using Data.Interfaces;
using Microsoft.Extensions.Configuration;
using Producer.Interfaces;
using Producer.Settings;
using System;
using System.IO;
using System.Text.Json;
using System.Threading;

namespace Producer
{
    /// <summary>
    /// Генератор сообщений. 
    /// </summary>
    internal class Producer : IProducer
    {
        private readonly ISender sender;
        private readonly int timeoutToSend;
        private readonly IDataGenerator<User> dataGenerator;

        public Producer()
        {
            var configuration = new ConfigurationBuilder()
                             .SetBasePath(Directory.GetCurrentDirectory())
                             .AddJsonFile("AppSettings.json")
                             .Build();

            this.dataGenerator = new FakeUserGenerator();

            var brokerSettings = configuration.GetSection("Broker").Get<BrokerSettings>();
            var exchangeSettings = configuration.GetSection("Broker").GetSection("Exchange").Get<ExchangeSettings>();
            var queueSettings = configuration.GetSection("Broker").GetSection("Queue").Get<QueueSettings>();
            this.timeoutToSend = configuration.GetValue<int>("TimeoutToSend");

            var routingKey = $"{exchangeSettings.Name} -> {queueSettings.Name}";
            var client = new RabbitMQClient(brokerSettings.Host, brokerSettings.VirtualHost, brokerSettings.Login, brokerSettings.Password);

            RabbitMQExchangeConfigurator.Configure(client, exchangeSettings.Name, exchangeSettings.Type);
            RabbitMQQueueConfigurator.Configure(client, queueSettings.Name, queueSettings.Durable);
            RabbitMQExchangeQueueBinder.Configure(client, exchangeSettings.Name, queueSettings.Name, routingKey);
            
            this.sender = new RabbitMQSender(client, exchangeSettings.Name, routingKey);
        }

        /// <summary>
        /// Получить сообщение для отправки.
        /// </summary>
        /// <returns></returns>
        private string GetMessage()
        {
            User user = dataGenerator.Generate();
            return JsonSerializer.Serialize(user);
        }

        /// <summary>
        /// Запускает отправку сообщений.
        /// </summary>
        public void Run()
        {
            while (true)
            {
                string message = GetMessage();
                this.sender.Send(message);
                Console.WriteLine($"{DateTime.Now}: Отправлено сообщение '{message}'");
                Thread.Sleep(this.timeoutToSend);
            }
        }
    }
}
