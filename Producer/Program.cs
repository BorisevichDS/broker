﻿using Producer.Interfaces;
using System;

namespace Producer
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IProducer producer = new Producer();
                producer.Run();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }

    }
}
