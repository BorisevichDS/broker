﻿using Core.Brokers.RabbitMQ;
using Producer.Interfaces;
using RabbitMQ.Client;
using System.Text;

namespace Producer
{
    /// <summary>
    /// Отправитель.
    /// </summary>
    internal class RabbitMQSender : ISender
    {
        private readonly RabbitMQClient client;
        private readonly string exchangeName;
        private readonly string routingKey;

        public RabbitMQSender(RabbitMQClient client, string exchangeName, string routingKey)
        {
            this.client = client;
            this.exchangeName = exchangeName;
            this.routingKey = routingKey;
        }

        /// <summary>
        /// Отправить сообщение.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        public void Send(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            this.client.Channel.BasicPublish(exchange: exchangeName, routingKey: routingKey, body: body);
        }
    }
}
