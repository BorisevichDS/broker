﻿namespace Producer.Settings
{
    /// <summary>
    /// Настройки exchange.
    /// </summary>
    internal class ExchangeSettings
    {
        /// <summary>
        /// Получает или задает название exchange.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получает или задает тип exchange.
        /// </summary>
        public string Type { get; set; }
    }
}
